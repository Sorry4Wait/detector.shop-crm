<?php

namespace app\models;

class ProductObject extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'product_object';
    }
}
