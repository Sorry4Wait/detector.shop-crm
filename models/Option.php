<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;
/**
 * This is the model class for table "option".
 *
 * @property int $id
 * @property string $name
 * @property int value_type_id
 * @property string $valueTypeLabel
 * @property
 */
class Option extends \yii\db\ActiveRecord
{
    const TYPE_STRING = 1;
    const TYPE_FLOAT = 3;
    const TYPE_TIME = 4;
    const TYPE_DATE = 5;


    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'option';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name',], 'string', 'max' => 255],
            [['name', 'value_type_id',], 'required'],
            [['name'], 'required'],
            [['name'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Параметр',
            'value_type_id' => 'Тип значения',
            'valueTypeLabel' => 'Тип значения',
        ];
    }

    private static $list = null;

    /**
     * @return array
     */
    public static function getList()
    {
        if (is_null(static::$list)) {
            static::$list = [
                static::TYPE_STRING => 'Текст',
                static::TYPE_FLOAT => 'Число (дробное)',
                static::TYPE_TIME => 'Время',
                static::TYPE_DATE => 'Дата'
            ];
        }
        return static::$list;
    }


    public function getOptionList()
    {
        $option = Option::find()->all();
        return ArrayHelper::map($option, 'id', 'name');
    }

    public function getValueTypeLabel()
    {
        return self::getList()[$this->value_type_id] ?? null;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getParameters()
    {
        return $this->hasMany(Parameter::className(), ['option_id' => 'id']);
    }
}
