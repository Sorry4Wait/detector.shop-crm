<?php

namespace app\models;

class EquipmentObject extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'equipment_object';
    }
}
