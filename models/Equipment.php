<?php

namespace app\models;

use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "equipment".
 *
 * @property int $id
 * @property string $name
 */
class Equipment extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public $object_list;
    public static function tableName()
    {
        return 'equipment';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name'], 'string', 'max' => 255],
            [['name'], 'required'],
            [['name'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Позиция оборудования',
            'objects' => 'Объект',
            'object_list' => 'Объекты',
        ];
    }

    public function getObjects()
    {
        return $this->hasMany(OurObject::className(), ['id' => 'object_id'])->viaTable('equipment_object', ['equipment_id' => 'id']);
    }

    public function getObjectsList()
    {
        $objects = OurObject::find()->all();

        return ArrayHelper::map($objects, 'id', 'name');
    }

    public function equipmentsObjectList()
    {
        $equipments = EquipmentObject::find()->where(['equipment_id' => $this->id])->all();

        $result = [];

        foreach ($equipments as $value) {
            $result [] = $value->object_id;
        }

        return $result;
    }

    private static $list = null;

    /**
     * @return array
     */
    public static function getList()
    {
        if (is_null(static::$list)) {
            static::$list = ArrayHelper::map(
                self::find()
                    ->orderBy('name')->all(),
                'id', 'name'
            );
        }

        return static::$list;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getParameters()
    {
        return $this->hasMany(Parameter::className(), ['equipment_id' => 'id']);
    }
}
