<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * UsersSearch represents the model behind the search form about `app\models\Users`.
 */
class JournalSearch extends Journal
{
    public $objectName;
    public $equipmentName;
    public $userName;
    public $typeName;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['object_id', 'equipment_id', 'user_id', 'type_id'], 'integer'],
            [['date'], 'safe'],
            [['description', 'media'], 'string', 'max' => 255],
            [['objectName', 'equipmentName', 'userName', 'typeName'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Journal::find()
            ->leftJoin(
                OurObject::tableName(), OurObject::tableName() . '.id=object_id'
            )
            ->leftJoin(
                Equipment::tableName(), Equipment::tableName() . '.id=equipment_id'
            )
            ->leftJoin(
                Users::tableName(), Users::tableName() . '.id=user_id'
            )
            ->leftJoin(
                Type::tableName(), Type::tableName() . '.id=type_id'
            );

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);


        $dataProvider->setSort([
            'attributes' => [
                'id',
                'object_id' => [
                    'asc' => [
                        'object.name' => SORT_ASC,
                    ],
                    'desc' => [
                        'object.name' => SORT_DESC,
                    ],
                    //'label' => 'Parent Name',
                    'default' => SORT_ASC
                ],
                'equipment_id' => [
                    'asc' => [
                        'equipment.name' => SORT_ASC,
                    ],
                    'desc' => [
                        'equipment.name' => SORT_DESC,
                    ],
                    //'label' => 'Оборудование',
                    'default' => SORT_ASC
                ],
                'user_id' => [
                    'asc' => [
                        Users::tableName() . '.name' => SORT_ASC,
                    ],
                    'desc' => [
                        Users::tableName() . '.name' => SORT_DESC,
                    ],
                    //'label' => 'Оборудование',
                    'default' => SORT_ASC
                ],
                'type_id' => [
                    'asc' => [
                        Type::tableName() . '.name' => SORT_ASC,
                    ],
                    'desc' => [
                        Type::tableName() . '.name' => SORT_DESC,
                    ],
                    //'label' => 'Оборудование',
                    'default' => SORT_ASC
                ],
                'date',

            ]
        ]);


        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'equipment_id' => $this->equipment_id,
            'object_id' => $this->object_id,
            'user_id' => $this->user_id,
            'type_id' => $this->type_id
        ]);

        //если надо будет реализовать текстовый поиск
//        $query->andFilterWhere(['like', OurObject::tableName() . '.name', $this->objectName]);
//        $query->andFilterWhere(['like', Equipment::tableName() . '.name', $this->equipmentName]);
//        $query->andFilterWhere(['like', Users::tableName() . '.name', $this->userName]);
//        $query->andFilterWhere(['like', Type::tableName() . '.name', $this->typeName]);

        $query->andFilterWhere(['like', 'description', $this->description]);
        $query->andFilterWhere(['like', 'date', $this->date]);

//        $query->andFilterWhere(['like', 'name', $this->name])
//            ->andFilterWhere(['like', 'login', $this->login])
//            ->andFilterWhere(['like', 'password', $this->password])
//            ->andFilterWhere(['like', 'telephone', $this->telephone])
//            ->andFilterWhere(['like', 'role_id', $this->role_id])
//            ->andFilterWhere(['like', 'status', $this->status])
//            ->andFilterWhere(['like', 'auth_key', $this->auth_key]);

        return $dataProvider;
    }
}
