<?php

namespace app\models;

use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "object".
 *
 * @property int $id
 * @property string $name
 */
class OurObject extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'object';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name'], 'string', 'max' => 255],
            [['name'], 'required'],
            [['name'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название объекта',
        ];
    }

    public function getEquipments()
    {
        return $this->hasMany(Equipment::className(), ['id' => 'equipment_id'])->viaTable('equipment_object', ['object_id' => 'id']);
    }

    public function getProducts()
    {
        return $this->hasMany(Product::className(), ['id' => 'product_id'])->viaTable('equipment_object', ['object_id' => 'id']);
    }

    private static $list = null;

    /**
     * @return array
     */
    public static function getList()
    {
        if (is_null(static::$list)) {
            static::$list = ArrayHelper::map(
                self::find()
                    ->orderBy('name')->all(),
                'id', 'name'
            );
        }

        return static::$list;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getParameters()
    {
        return $this->hasMany(Parameter::className(), ['object_id' => 'id']);
    }
}
