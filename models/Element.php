<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;
/**
 * This is the model class for table "element".
 *
 * @property integer $id
 * @property integer $object_id
 * @property integer $equipment_id
 * @property integer $element_number
 * @property integer $element_row
 * @property integer $number
 * @property string $modification
 * @property string $date_ust
 * @property string $date_dem
 * @property string $dem_reason
 * @property string $comment
 * @property integer $user_id
 *
 * @property OurObject $object
 * @property Equipment $equipment
 * @property Users $user
 */
class Element extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'element';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['object_id', 'equipment_id', 'element_number', 'element_row', 'number', 'user_id'], 'integer'],
            [['date_ust', 'date_dem'], 'safe'],
            [['modification', 'dem_reason', 'comment'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'object_id' => 'Объект',
            'equipment_id' => 'Оборудование',
            'element_number' => 'Расположение элемента (номер диска)',
            'element_row' => ' Расположение элемента (номер ряда)',
            'number' => 'Номер элемента',
            'modification' => 'Модификация элемента',
            'date_ust' => 'Дата установки',
            'date_dem' => 'Дата демонтажа',
            'dem_reason' => ' Причина демонтажа',
            'comment' => 'Примечание',
            'user_id' => 'User ID',
        ];
    }

    public function getEquipment()
    {
        return $this->hasOne(Equipment::class, ['id' => 'equipment_id']);
    }

    public function getObject()
    {
        return $this->hasOne(OurObject::class, ['id' => 'object_id']);
    }

    public function getUser()
    {
        return $this->hasOne(Users::class, ['id' => 'user_id']);
    }

    public function getEquipmentByObjectID($id)
    {
        $equipmentsObject = EquipmentObject::find()->where(['object_id' => $id])->all();
        $equipments = [];

        foreach ($equipmentsObject as $equipment) {
            $equipments[$equipment['equipment_id']] = $this->getLabelForEquipmentByID($equipment['equipment_id']);
        }

        return $equipments;
    }

    public function getEquipmentList($object_id)
    {
        $datas = EquipmentObject::find()->where(['object_id' => $object_id ])->all();
        $array = [];
        foreach($datas as $data){
            $array [] = $data->equipment_id;
        }
        $array = array_unique($array);
        $equipments = Equipment::find()->where(['id' => $array])->all();
        return ArrayHelper::map($equipments, 'id', 'name');
    }


}
