<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "users".
 *
 * @property int $id
 * @property string $fio
 * @property string $login
 * @property string $password
 * @property int $role_id
 */

class Users extends \yii\db\ActiveRecord
{
    const USER_ROLE_ADMIN = 1;
    const USER_ROLE_OPERATOR = 2;
    const USER_ROLE_TEH_SPEC = 3;
    public $new_password;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'users';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['fio', 'password', 'login'], 'required'],
            [['login'], 'email'],
            [['login'], 'unique'],
            [['role_id'], 'integer'],
            [['fio', 'login', 'password', 'new_password'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'fio' => 'ФИО',
            'login' => 'Логин',
            'password' => 'Пароль',
            'role_id' => 'Должность',
            'new_password' => 'Новый пароль',
        ];
    }

    public function beforeSave($insert)
    {
        if ($this->isNewRecord) {
            $this->password = md5($this->password);
        }

        if ($this->new_password != null) {
            $this->password = md5($this->new_password);
        }
        return parent::beforeSave($insert);
    }

    public function getRoleList()
    {
        return ArrayHelper::map([
            ['id' => self::USER_ROLE_ADMIN, 'name' => 'Администратор',],
            ['id' => self::USER_ROLE_OPERATOR, 'name' => 'Оператор',],
            ['id' => self::USER_ROLE_TEH_SPEC, 'name' => 'Тех. специалист',],
        ], 'id', 'name');
    }

    public function getRoleDescription()
    {
        if($this->role_id == self::USER_ROLE_ADMIN) return 'Администратор';
        if($this->role_id == self::USER_ROLE_OPERATOR) return 'Оператор';
        if($this->role_id == self::USER_ROLE_TEH_SPEC) return 'Тех. специалист';
    }

    public function getAtelierList()
    {
        $ateliers = Atelier::find()->all();
        return ArrayHelper::map($ateliers, 'id', 'name');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getParameters()
    {
        return $this->hasMany(Parameter::className(), ['user_id' => 'id']);
    }

}
