<?php

use yii\db\Migration;

/**
 * Handles the creation of table `user`.
 */
class m180818_103354_create_user_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('users', [
            'id' => $this->primaryKey(),
            'fio' => $this->string(255),
            'login' => $this->string(255),
            'password' => $this->string(255),
            'role_id' => $this->integer(),
        ]);

        $this->insert('users',array(
            'fio' => 'Иванов Иван Иванович',          
            'login' => 'admin',
            'password' => md5('admin'),
            'role_id' => 1,//administrator
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('user');
    }
}
