<?php

use yii\db\Migration;

/**
 * Class m181019_092225_alter_table_journal
 */
class m181019_092225_alter_table_journal extends Migration
{



    // Use up()/down() to run migration code without a transaction.
    public function up()
    {
        $this->addColumn('journal', 'node_name', $this->string()->comment('Наименование узла'));
    }

    public function down()
    {
        echo "m181019_092225_alter_table_journal cannot be reverted.\n";

        return false;
    }

}
