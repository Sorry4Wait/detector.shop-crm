<?php

use yii\db\Migration;

/**
 * Handles the creation of table `product_object`.
 */
class m180820_132046_create_product_object_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('product_object', [
            'id' => $this->primaryKey(),
            'product_id' => $this->integer(),
            'object_id' => $this->integer(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('product_object');
    }
}
