<?php

use yii\db\Migration;

/**
 * Handles the creation of table `parameter`.
 */
class m180824_135947_create_parameter_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('parameter', [
            'id' => $this->primaryKey(),
            'object_id' => $this->integer(),
            'user_id' => $this->integer(),
            'equipment_id' => $this->integer(),
            'date' => $this->dateTime(),
            'product_id' => $this->integer(),
            'option_id' => $this->integer(),
            'value' => $this->string(255),
            'number' => $this->integer(),
        ], 'CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE=InnoDB');

        $this->createIndex('idx-parameter-object_id', 'parameter', 'object_id', false);
        $this->addForeignKey("fk-parameter-object_id", "parameter", "object_id", "object", "id");

        $this->createIndex('idx-parameter-user_id', 'parameter', 'user_id', false);
        $this->addForeignKey("fk-parameter-user_id", "parameter", "user_id", "users", "id");

        $this->createIndex('idx-parameter-equipment_id', 'parameter', 'equipment_id', false);
        $this->addForeignKey("fk-parameter-equipment_id", "parameter", "equipment_id", "equipment", "id");

        $this->createIndex('idx-parameter-product_id', 'parameter', 'product_id', false);
        $this->addForeignKey("fk-parameter-product_id", "parameter", "product_id", "product", "id");

        $this->createIndex('idx-parameter-option_id', 'parameter', 'option_id', false);
        $this->addForeignKey("fk-parameter-option_id", "parameter", "option_id", "option", "id");
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
    	$this->dropForeignKey('fk-parameter-object_id','parameter');
        $this->dropIndex('idx-parameter-object_id','parameter');

        $this->dropForeignKey('fk-parameter-user_id','parameter');
        $this->dropIndex('idx-parameter-user_id','parameter');

        $this->dropForeignKey('fk-parameter-equipment_id','parameter');
        $this->dropIndex('idx-parameter-equipment_id','parameter');

        $this->dropForeignKey('fk-parameter-product_id','parameter');
        $this->dropIndex('idx-parameter-product_id','parameter');

        $this->dropForeignKey('fk-parameter-option_id','parameter');
        $this->dropIndex('idx-parameter-option_id','parameter');

        $this->dropTable('parameter');
    }
}
