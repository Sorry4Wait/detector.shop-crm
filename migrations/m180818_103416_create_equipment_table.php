<?php

use yii\db\Migration;

/**
 * Handles the creation of table `equipment`.
 */
class m180818_103416_create_equipment_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('equipment', [
            'id' => $this->primaryKey(),
            'name' => $this->string(),
        ], 'CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE=InnoDB');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('equipment');
    }
}
