<?php

namespace app\controllers;

use app\models\Parameter;
use Yii;
use yii\data\ActiveDataProvider;
use yii\filters\VerbFilter;
use yii\helpers\Html;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use app\models\ProductObject;
use app\models\Product;
use app\models\EquipmentObject;
use app\models\Equipment;
use app\models\Option;

/**
 * ParameterController implements the CRUD actions for Parameter model.
 */
class ParameterController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                    'bulk-delete' => ['post'],
                ],
            ],
        ];
    }

    public function actionLists($id)
    {
        $datas = ProductObject::find()->where(['object_id' => $id ])->all();
        foreach($datas as $data){
            $product = Product::findOne($data->product_id);
            echo "<option value = '".$product->id."'>".$product->name."</option>" ;
        }
    }

    public function actionListsEquipment($id)
    {
        $datas = EquipmentObject::find()->where(['object_id' => $id ])->all();
        $array = [];
        foreach($datas as $data){
            $array [] = $data->equipment_id;
        }
        $array = array_unique($array);

        foreach($array as $data){
            $equipment = Equipment::findOne($data);
            echo "<option value = '".$equipment->id."'>".$equipment->name."</option>" ;
        }
    }

    public function actionOption($id)
    {
        $option = Option::findOne($id);
        if($option->value_type_id == 1) return 'string';
        if($option->value_type_id == 3) return 'number';
    }

    /**
     * Lists all Parameter models.
     *
     * @return mixed
     */
    public function actionIndex()
    {

        $request = Yii::$app->request;
        $model = new Parameter();
        $dataProvider = new ActiveDataProvider([
            'query' => Parameter::find(),
            'pagination' => false,
        ]);

        /*        if ($request->isAjax) {
                    Yii::$app->response->format = Response::FORMAT_JSON;
                    if ($request->isGet) {
                        return [
                            'title' => "Создать новый параметр",
                            'content' => $this->renderAjax('index', [
                                'model' => $model,
                                'dataProvider' => $dataProvider,
                            ]),
                            'footer' => Html::button('Close', [
                                    'class' => 'btn btn-default pull-left',
                                    'data-dismiss' => "modal",
                                ]) .
                                        Html::button('Save', ['class' => 'btn btn-primary', 'type' => "submit"]),

                        ];
                    } else {
                        if ($model->load($request->post()) && $model->save()) {
                            return [
                                'forceReload' => '#crud-datatable-pjax',
                                'title' => "Создать новый параметр",
                                'content' => '<span class="text-success">Create Parameter success</span>',
                                'footer' => Html::button('Close', [
                                        'class' => 'btn btn-default pull-left',
                                        'data-dismiss' => "modal",
                                    ]) .
                                            Html::a('Create More', ['create'], [
                                                'class' => 'btn btn-primary',
                                                'role' => 'modal-remote',
                                            ]),

                            ];
                        } else {
                            return [
                                'title' => "Создать новый параметр",
                                'content' => $this->renderAjax('index', [
                                    'model' => $model,
                                    'dataProvider' => $dataProvider,
                                ]),
                                'footer' => Html::button('Close', [
                                        'class' => 'btn btn-default pull-left',
                                        'data-dismiss' => "modal",
                                    ]) .
                                            Html::button('Save', ['class' => 'btn btn-primary', 'type' => "submit"]),

                            ];
                        }
                    }
                } else {
                    if ($model->load($request->post())) {
                        $model->save();
                        return $this->redirect(Yii::$app->request->referrer);
        //                return $this->redirect(['view', 'id' => $model->id]);
                    } else {
                        return $this->render('index', [
                            'model' => $model,
                            'dataProvider' => $dataProvider,
                        ]);
                    }
                }
        */


        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }


    /**
     * Displays a single Parameter model.
     *
     * @param integer $id
     *
     * @return mixed
     */
    /*public function actionView($id)
    {
        $request = Yii::$app->request;
        if ($request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;

            return [
                'title' => "Parameter #" . $id,
                'content' => $this->renderAjax('view', [
                    'model' => $this->findModel($id),
                ]),
                'footer' => Html::button('Close', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                            Html::a('Edit', ['update', 'id' => $id], [
                                'class' => 'btn btn-primary',
                                'role' => 'modal-remote',
                            ]),
            ];
        } else {
            return $this->render('view', [
                'model' => $this->findModel($id),
            ]);
        }
    }
*/
    /**
     * Creates a new Parameter model.
     * For ajax request will return json object
     * and for non-ajax request if creation is successful, the browser will be redirected to the 'view' page.
     *
     * @return mixed
     */
    public function actionCreate($id = null, $object_id = null, $equipment_id = null, $product_id = null)
    {

        $request = Yii::$app->request;
        $model = new Parameter();
        $model->object_id = $object_id;
        $model->equipment_id = $equipment_id;
        $model->product_id = $product_id;

        if ($id > 0)
        {
            $query = Parameter::find()->where(['number'=>$id]);
            $model->number = $id;
        }
        else $query = Parameter::find()->where(['id'=>'null']);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort'=>array(
                'defaultOrder'=>['id' => SORT_DESC],
            ),
            'pagination' => [
                'pageSize' => 10,
                'validatePage' => false,
            ],
        ]);

        if (!empty($_POST))
        {

            $arr = $_POST;
            unset($arr['_csrf']);
            unset($arr['Parameter']);
            $count = 0;
            $numb = 0;
            foreach ($arr as $key => $value){
                if(!empty($value)){
                    $count++;
                    $a = explode('Paramaa-',$key);
                    $option_id = (int)$a[1];

                    $mode = new Parameter();
                    $mode->object_id = (int)$_POST['Parameter']['object_id'];
                    $mode->equipment_id = (int)$_POST['Parameter']['equipment_id'];
                    $mode->product_id = (int)$_POST['Parameter']['product_id'];
                    $mode->option_id = $option_id;
                    $mode->value = $value;
                    $mode->save();

                    if($count == 1){
                        $mode->number = $mode->id;
                        $numb  = $mode->number;
                        $mode->save();
                    }else{
                        $mode->number = $numb;
                        $mode->save();
                    }
                }


            }



            return $this->redirect(['create', 'id' =>$numb, 'object_id' => (int)$_POST['Parameter']['object_id'], 'equipment_id' => (int)$_POST['Parameter']['equipment_id'], 'product_id' => (int)$_POST['Parameter']['product_id']]);
        }
        else
        {
            unset($_SESSION['numb']);
            return $this->render('create', [
                'model' => $model,
                'dataProvider' => $dataProvider,
            ]);
        }
    }

    /**
     * Updates an existing Parameter model.
     * For ajax request will return json object
     * and for non-ajax request if update is successful, the browser will be redirected to the 'view' page.
     *
     * @param integer $id
     *
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $request = Yii::$app->request;
        $model = $this->findModel($id);

        Yii::$app->response->format = Response::FORMAT_JSON;
        if ($model->load($request->post()) && $model->save()) {
            return [
                'forceReload' => '#crud-datatable-pjax',
                'title' => "Параметр",
                'forceClose' => true,
            ];
        } else {
            return [
                'title' => "Изменить",
                'content' => $this->renderAjax('update', [
                    'model' => $model,
                ]),
                'footer' => Html::button('Отмена', [ 'class' => 'btn btn-default pull-left', 'data-dismiss' => "modal", ]) .
                    Html::button('Сохранить', ['class' => 'btn btn-primary', 'type' => "submit"]),
            ];
        }
    }

    /**
     * Delete an existing Parameter model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     *
     * @param integer $id
     *
     * @return mixed
     */
    public function actionDelete($id)
    {
        $request = Yii::$app->request;
        $number = $this->findModel($id)->number;
        $this->findModel($id)->delete();

        if ($request->isAjax) {
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;

            return ['forceClose' => true, 'forceReload' => '#crud-datatable-pjax'];
        } else {
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['create?id='.$number]);
        }
    }

    public function actionDeleteAll($number)
    {
        $request = Yii::$app->request;
        $models = Parameter::find()->where(['number' => $number])->all();
        foreach ($models as $value) {
            $value->delete();
        }
        if ($request->isAjax) {
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;

            return ['forceClose' => true, 'forceReload' => '#available-pjax'];
        } else {
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }
    }

    /**
     * Finds the Parameter model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     *
     * @param integer $id
     *
     * @return Parameter the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Parameter::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}