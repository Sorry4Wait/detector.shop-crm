<?php

namespace app\controllers;

use app\models\OurObject;
use Yii;
use yii\data\ActiveDataProvider;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\helpers\Html;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use app\models\Parameter;
use app\models\EquipmentObject;
use app\models\ProductObject;

/**
 * ObjectController implements the CRUD actions for Object model.
 */
class ObjectController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'rules' => [
                   [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                    'bulk-delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Object models.
     *
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => OurObject::find(),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Object model.
     *
     * @param integer $id
     *
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        $request = Yii::$app->request;
        if($request->isAjax){
            Yii::$app->response->format = Response::FORMAT_JSON;
            return [
                'title'=> "Объект",
                'size' => 'normal',
                'content'=>$this->renderAjax('/object/view', [
                    'model' => $this->findModel($id),
                ]),
                'footer'=> Html::button('Отмена',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                           Html::a('Изменить',['update','id'=>$id],['class'=>'btn btn-primary','role'=>'modal-remote'])
            ];
        }else{
            return $this->render('/object/view-object', [
                'model' => $this->findModel($id),
            ]);
        }
    }

    /**
     * Creates a new Object model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     *
     * @return mixed
     */
    public function actionCreate()
    {
        $request = Yii::$app->request;
        $model = new OurObject();  

        if($request->isAjax){
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($model->load($request->post()) && $model->save()){
                return [
                    'forceReload'=>'#crud-datatable-pjax',
                    'title'=> "Пользователи",
                    'size' => 'normal',
                    'content'=>'<span class="text-success">Успешно выполнено</span>',
                    'footer'=> Html::button('Ок',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                            Html::a('Создать ещё',['create'],['class'=>'btn btn-primary','role'=>'modal-remote'])
        
                ];         
            }else{           
                return [
                    'title'=> "Создать",
                    'size' => 'large',
                    'content'=>$this->renderAjax('create', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Отмена',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('Сохранить',['class'=>'btn btn-primary','type'=>"submit"])
        
                ];         
            }
        }else{

            if ($model->load($request->post()) && $model->save()) {
                return $this->redirect(['index']);
            } else {
                return $this->render('create', [
                    'model' => $model,
                ]);
            }
        }
       
    }

    /**
     * Updates an existing Object model.
     * If update is successful, the browser will be redirected to the 'view' page.
     *
     * @param integer $id
     *
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $request = Yii::$app->request;
        $model = $this->findModel($id);

        if($request->isAjax){
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($model->load($request->post()) && $model->save()){
                return [
                    'forceReload'=>'#crud-datatable-pjax',
                    'title'=> "Объект",
                    'size' => 'normal',
                    'content'=>$this->renderAjax('/object/view', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Отмена',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                               Html::a('Изменить',['update','id'=>$id],['class'=>'btn btn-primary','role'=>'modal-remote'])
                ];
            }else{
                return [
                    'title'=> "Изменить",
                    'size' => 'normal',
                    'content'=>$this->renderAjax('/object/update', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Отмена',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                               Html::button('Сохранить',['class'=>'btn btn-primary','type'=>"submit"])
                ];
            }
        }else{
            /*
            *   Process for non-ajax request
            */
            if ($model->load($request->post()) && $model->save()) {
                return $this->redirect(['index']);
            } else {
                return $this->render('/object/update', [
                    'model' => $model,
                ]);
            }
        }
    }

    /**
     * Deletes an existing Object model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     *
     * @param integer $id
     *
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $request = Yii::$app->request;
        Yii::$app->response->format = Response::FORMAT_JSON;
        $parameter = Parameter::find()->where(['object_id'=> $id])->one();             
        if( $parameter != null) 
        {          
            return [
                'title'=> "".'<b>Ошибка</b>',
                'content'=>'<center><span class="text-success" style="color:red; font-size:20px;"><b>Вы не можете удалить эту объект. Потому что он связано с параметрами!!!</b></span></center>',                              
            ];
        }

        $equipment = EquipmentObject::find()->where(['object_id'=> $id])->one();             
        if( $equipment != null) 
        {          
            return [
                'title'=> "".'<b>Ошибка</b>',
                'content'=>'<center><span class="text-success" style="color:red; font-size:20px;"><b>Вы не можете удалить эту объект. Потому что он связано с оборудованиями!!!</b></span></center>',                              
            ];
        }

        $product_object = ProductObject::find()->where(['object_id'=> $id])->one();             
        if( $product_object != null) 
        {          
            return [
                'title'=> "".'<b>Ошибка</b>',
                'content'=>'<center><span class="text-success" style="color:red; font-size:20px;"><b>Вы не можете удалить эту объект. Потому что он связано с продуктами!!!</b></span></center>',                              
            ];
        }
        $this->findModel($id)->delete();

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            return ['forceClose'=>true,'forceReload'=>'#crud-datatable-pjax'];
        }else{
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }
    }

    /**
     * Delete multiple existing Product model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionBulkDelete()
    {
        $request = Yii::$app->request;
        $pks = explode(',', $request->post( 'pks' )); // Array or selected records primary keys
        foreach ( $pks as $pk ) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            $model = $this->findModel($pk);
            $parameter = Parameter::find()->where(['object_id'=> $pk])->one();             
            if( $parameter != null) 
            {          
                return [
                    'title'=> "".'<b>Ошибка</b>',
                    'content'=>'<center><span class="text-success" style="color:red; font-size:20px;"><b>Вы не можете удалить этот объект. Потому что он связань с параметрами!!!</b></span></center>',                              
                ];
            }

            $equipment = EquipmentObject::find()->where(['object_id'=> $pk])->one();             
            if( $equipment != null) 
            {          
                return [
                    'title'=> "".'<b>Ошибка</b>',
                    'content'=>'<center><span class="text-success" style="color:red; font-size:20px;"><b>Вы не можете удалить этот объект. Потому что он связань с оборудованиями!!!</b></span></center>',                              
                ];
            }

            $product_object = ProductObject::find()->where(['object_id'=> $pk])->one();             
            if( $product_object != null) 
            {          
                return [
                    'title'=> "".'<b>Ошибка</b>',
                    'content'=>'<center><span class="text-success" style="color:red; font-size:20px;"><b>Вы не можете удалить этот объект. Потому что он связань с продуктами!!!</b></span></center>',                              
                ];
            }
            $model->delete();
        }

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            return ['forceClose'=>true,'forceReload'=>'#crud-datatable-pjax'];
        }else{
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }

    }

    /**
     * Finds the Object model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     *
     * @param integer $id
     *
     * @return OurObject the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = OurObject::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
