<?php

namespace app\controllers;

use app\models\Product;
use app\models\ProductObject;
use Yii;
use yii\data\ActiveDataProvider;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\helpers\Html;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use app\models\Parameter;

/**
 * ProductController implements the CRUD actions for Product model.
 */
class ProductController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'rules' => [
                   [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Product models.
     *
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Product::find(),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }


    /**
     * Displays a single Product model.
     *
     * @param integer $id
     *
     * @return mixed
     */
    public function actionView($id)
    {
        $request = Yii::$app->request;
        if ($request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;

            return [
                'title' => "Продукт",
                'size' => 'normal',
                'content' => $this->renderAjax('/product/view', [
                    'model' => $this->findModel($id),
                ]),
                'footer' => Html::button('Отмена', ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"])
                            .
                            Html::a('Изменить', ['update', 'id' => $id], [
                                'class' => 'btn btn-primary',
                                'role' => 'modal-remote',
                            ]),
            ];
        } else {
            return $this->render('/product/view', [
                'model' => $this->findModel($id),
            ]);
        }
    }

    /**
     * Creates a new Product model.
     * For ajax request will return json object
     * and for non-ajax request if creation is successful, the browser will be redirected to the 'view' page.
     *
     * @return mixed
     */
    public function actionCreate()
    {
        $request = Yii::$app->request;
        $model = new Product();

        if ($request->isAjax) {
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if ($request->isGet) {
                return [
                    'title' => "Создать",
                    'size' => 'large',
                    'content' => $this->renderAjax('/product/create', [
                        'model' => $model,
                    ]),
                    'footer' => Html::button('Отмена', [
                            'class' => 'btn btn-default pull-left',
                            'data-dismiss' => "modal",
                        ]) .
                                Html::button('Сохранить', ['class' => 'btn btn-primary', 'type' => "submit"]),

                ];
            } else {
                if ($model->load($request->post()) && $model->save()) {
                    foreach ($request->post()['Product']['objects'] as $value) {
                        $projectsUser = new ProductObject();
                        $projectsUser->product_id = $model->id;
                        $projectsUser->object_id = $value;
                        $projectsUser->save();
                    }

                    return [
                        'forceReload' => '#crud-datatable-pjax',
                        'title' => "Продукты",
                        'size' => 'normal',
                        'content' => '<span class="text-success">Успешно выполнено</span>',
                        'footer' => Html::button('Ок', [
                                'class' => 'btn btn-default pull-left',
                                'data-dismiss' => "modal",
                            ]) .
                                    Html::a('Создать ещё', ['create'], [
                                        'class' => 'btn btn-primary',
                                        'role' => 'modal-remote',
                                    ]),

                    ];
                } else {
                    return [
                        'title' => "Создать",
                        'size' => 'large',
                        'content' => $this->renderAjax('/product/create', [
                            'model' => $model,
                        ]),
                        'footer' => Html::button('Отмена', [
                                'class' => 'btn btn-default pull-left',
                                'data-dismiss' => "modal",
                            ]) .
                                    Html::button('Сохранить', ['class' => 'btn btn-primary', 'type' => "submit"]),

                    ];
                }
            }
        } else {
            /*
            *   Process for non-ajax request
            */
            if ($model->load($request->post()) && $model->save()) {
                return $this->redirect(['index']);
            } else {
                return $this->render('/product/create', [
                    'model' => $model,
                ]);
            }
        }
    }

    /**
     * Updates an existing Product model.
     * For ajax request will return json object
     * and for non-ajax request if update is successful, the browser will be redirected to the 'view' page.
     *
     * @param integer $id
     *
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $request = Yii::$app->request;
        $model = $this->findModel($id);

        if ($request->isAjax) {
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if ($request->isGet) {
                return [
                    'title' => "Изменить",
                    'size' => 'large',
                    'content' => $this->renderAjax('/product/update', [
                        'model' => $model,
                    ]),
                    'footer' => Html::button('Отмена', [
                            'class' => 'btn btn-default pull-left',
                            'data-dismiss' => "modal",
                        ]) .
                                Html::button('Сохранить', ['class' => 'btn btn-primary', 'type' => "submit"]),
                ];
            } else {
                if ($model->load($request->post()) && $model->save()) {
                    
                    $objects = ProductObject::find()->where(['product_id' => $model->id])->all();
                    foreach ($objects as $value) {
                        $value->delete();
                    }

                    foreach ($request->post()['Product']['objects'] as $value) {
                        $projectsUser = new ProductObject();
                        $projectsUser->product_id = $model->id;
                        $projectsUser->object_id = $value;
                        $projectsUser->save();
                    }

                    return [
                        'forceReload' => '#crud-datatable-pjax',
                        'title' => "Продукты",
                        'size' => 'normal',
                        'content' => $this->renderAjax('/product/view', [
                            'model' => $model,
                        ]),
                        'footer' => Html::button('Отмена', [
                                'class' => 'btn btn-default pull-left',
                                'data-dismiss' => "modal",
                            ]) .
                                    Html::a('Изменить', ['update', 'id' => $id], [
                                        'class' => 'btn btn-primary',
                                        'role' => 'modal-remote',
                                    ]),
                    ];
                } else {
                    return [
                        'title' => "Изменить",
                        'size' => 'large',
                        'content' => $this->renderAjax('/product/update', [
                            'model' => $model,
                        ]),
                        'footer' => Html::button('Отмена', [
                                'class' => 'btn btn-default pull-left',
                                'data-dismiss' => "modal",
                            ]) .
                                    Html::button('Сохранить', ['class' => 'btn btn-primary', 'type' => "submit"]),
                    ];
                }
            }
        } else {
            /*
            *   Process for non-ajax request
            */
            if ($model->load($request->post()) && $model->save()) {
                return $this->redirect(['index']);
            } else {
                return $this->render('/product/update', [
                    'model' => $model,
                ]);
            }
        }
    }

    /**
     * Delete an existing Product model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     *
     * @param integer $id
     *
     * @return mixed
     */
    public function actionDelete($id)
    {
        $request = Yii::$app->request;
        Yii::$app->response->format = Response::FORMAT_JSON;
        $parameter = Parameter::find()->where(['product_id'=> $id])->one();             
        if( $parameter != null) 
        {          
            return [
                'title'=> "".'<b>Ошибка</b>',
                'content'=>'<center><span class="text-success" style="color:red; font-size:20px;"><b>Вы не можете удалить этот продукт. Потому что он связань с параметрами!!!</b></span></center>',                              
            ];
        }
        $product = ProductObject::find()->where(['product_id'=> $id])->all();             
        foreach ($product as $value) {
            $value->delete();
        }

        $this->findModel($id)->delete();

        if ($request->isAjax) {
            /*
            *   Process for ajax request
            */

            return ['forceClose' => true, 'forceReload' => '#crud-datatable-pjax'];
        } else {
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }
    }

    /**
     * Delete multiple existing Product model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     *
     * @param integer $id
     *
     * @return mixed
     */
    public function actionBulkDelete()
    {
        $request = Yii::$app->request;
        $pks = explode(',', $request->post('pks')); // Array or selected records primary keys
        foreach ($pks as $pk) {
            $model = $this->findModel($pk);
            $model->delete();
        }

        if ($request->isAjax) {
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;

            return ['forceClose' => true, 'forceReload' => '#crud-datatable-pjax'];
        } else {
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }
    }

    /**
     * Finds the Product model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     *
     * @param integer $id
     *
     * @return Product the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Product::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
