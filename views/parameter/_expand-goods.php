<?php

use kartik\grid\GridView;
use yii\helpers\Url;
use yii\helpers\Html;
use app\models\Parameter;
use yii\data\ActiveDataProvider;

/*$session = Yii::$app->session;
$result = [];
foreach ($session['provider']->getModels() as $model) {
    if($model->number == $number) $result [] = $model->id;
}

$provider1 = AvailableSearch::searchById($result);*/

$query = Parameter::find()->where(['number' => $number]);

$provider1 = new ActiveDataProvider([
    'query' => $query,
    'sort'=>array(
        'defaultOrder'=>['id' => SORT_DESC],
    ),
]);
?>

<?=GridView::widget([
    'id'=>'crud-datatable',
    'dataProvider' => $provider1,
    'columns' => [
        [
            'class' => 'kartik\grid\SerialColumn',
            'width' => '30px',
        ],
        [
            'class'=>'\kartik\grid\DataColumn',
            'attribute'=>'product_id',
            'content' => function($data){
                return $data->product->name;
            }
        ],
        [
            'class'=>'\kartik\grid\DataColumn',
            'attribute'=>'option_id',
            'content' => function($data){
                return $data->option->name;
            }
        ],
        [
            'class'=>'\kartik\grid\DataColumn',
            'attribute'=>'value',
        ],
    ],
    'striped' => true,
    'condensed' => true,
    'responsive' => true,
])?>
