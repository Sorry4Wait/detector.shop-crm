<?php

use johnitvn\ajaxcrud\BulkButtonWidget;
use johnitvn\ajaxcrud\CrudAsset;
use kartik\grid\GridView;
use yii\bootstrap\Modal;
use yii\helpers\Html;
use yii\helpers\Url;
use app\models\Parameter;
use yii\data\ActiveDataProvider;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Параметры';
$this->params['breadcrumbs'][] = $this->title;

CrudAsset::register($this);

$numbers = [];
$models_id = [];
$result = [];
$count = 0;
foreach ($dataProvider->getModels() as $model)
{
    $count ++;
    $numbers [] = $model->number;
    $models_id [] = $model->id;
}
$numbers = array_unique($numbers);

foreach ($numbers as $number) {
    $available = Parameter::find()->where(['number' => $number])->one();
    if($available != null) $result [] = $available->id;
}

$query = Parameter::find()->where(['id' => $result]);
$provider = new ActiveDataProvider([
    'query' => $query,
    'sort'=>array(
        'defaultOrder'=>['id' => SORT_DESC],
    ),
]);
?>
<div class="parameter-index">
    <div id="ajaxCrudDatatable">
        <?= GridView::widget([
            'id'=>'available',
            'dataProvider' => $provider,
            //'filterModel' => $searchModel,
            'rowOptions' => function ($model, $key, $index, $grid) {
                return ['data-id' => $model->id];
            },
            'pjax'=>true,
            'columns' => [
                [
                    'class' => 'kartik\grid\SerialColumn',
                    'width' => '30px',
                ],
                [
                    'class'=>'kartik\grid\ExpandRowColumn', 
                    'width'=>'50px',
                    'value'=>function ($model, $key, $index, $column) {
                        return GridView::ROW_COLLAPSED;
                    },
                    'detail'=>function ($model, $key, $index, $column) {

                        return \Yii::$app->controller->renderPartial('_expand-goods', ['number'=>$model->number, /*'provider' => $provider*/]);
                    },
                    'headerOptions'=>['class'=>'kartik-sheet-style'],
                    'expandOneOnly'=>true
                ],
                [
                    'class'=>'\kartik\grid\DataColumn',
                    'attribute'=>'object_id',
                    'content' => function($data){
                        return $data->object->name;
                    }
                ],
                [
                    'class'=>'\kartik\grid\DataColumn',
                    'attribute'=>'user_id',
                    'content' => function($data){
                        return $data->user->fio;
                    }
                ],
                [
                    'class'=>'\kartik\grid\DataColumn',
                    'attribute'=>'equipment_id',
                    'content' => function($data){
                        return $data->equipment->name;
                    }
                ],
                [
                    'class'=>'\kartik\grid\DataColumn',
                    'attribute'=>'date',
                ],
                [
                    'class'    => 'kartik\grid\ActionColumn',
                    'template' => ' {leadView} {leadDelete}',
                    'buttons'  => [

                        'leadView' => function ($url, $model) {
                            $url = Url::to(['/parameter/create', 'id' => $model->number]);
                            return Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url, ['data-pjax'=>'0','title'=>'Изменить', 'data-toggle'=>'tooltip']);
                        },
                        'leadDelete' => function ($url, $model) {
                            $url = Url::to(['/parameter/delete-all', 'number' => $model->number]);
                            return Html::a('<span class="glyphicon glyphicon-trash"></span>', $url, [
                                'role'=>'modal-remote','title'=>'Удалить', 
                                      'data-confirm'=>false, 'data-method'=>false,
                                      'data-request-method'=>'post',
                                      'data-toggle'=>'tooltip',
                                      'data-confirm-title'=>'Подтвердите действие',
                                      'data-confirm-message'=>'Вы уверены что хотите удалить этого элемента?',
                            ]);
                        },
                    ]
                ]
            ],
            'toolbar'=> [
                ['content'=>
                    Html::a('Создать', ['create'],
                        ['data-pjax'=>'0','title'=> 'Создать','class'=>'btn btn-primary']).
                    Html::a('<i class="glyphicon glyphicon-repeat"></i>', [''],
                    ['data-pjax'=>1, 'class'=>'btn btn-default', 'title'=>'Обновить']).
                    '{toggleData}'
                ],
            ], 
            'striped' => true,
            'condensed' => true,
            'responsive' => true,
            'panel' => [
                'type' => 'primary', 
                'heading' => '<i class="glyphicon glyphicon-list"></i> Список',
                'before'=>'',
                'after'=>'',
            ]
        ])?>
    </div>
</div>
<script>
    $(document).ready(function () {
        $('#parameter-object_id').change(function () {
            $('#parameter-equipment_id option').hide();
            $('select').find('.equipment_object_' + $('#parameter-object_id').val()).show();
        });
    });
</script>
<?php Modal::begin([
    "id"=>"ajaxCrudModal",
    "footer"=>"",// always need it for jquery plugin
])?>
<?php Modal::end(); ?>
