<?php

/*use yii\helpers\Html;
use yii\grid\GridView;
*/
use johnitvn\ajaxcrud\CrudAsset;
use kartik\grid\GridView;
use yii\bootstrap\Modal;
use yii\helpers\Html;
use yii\helpers\Url;


CrudAsset::register($this);
/* @var $this yii\web\View */
/* @var $model app\models\Available */
$this->title = 'Поступление:';
?>
<div class="available-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>

		    <?=GridView::widget([
	            'id'=>'crud-datatable',
	            'dataProvider' => $dataProvider,
	            'pjax'=>true,
	            'columns' => [
	            	[
					    'class' => 'kartik\grid\SerialColumn',
					    'width' => '30px',
					],
		            [
						'attribute' => 'object_id',	
						'value'=>'object.name',
					],			
					[
						'attribute' => 'equipment_id',
						'value'=>'equipment.name',
					],
					[
						'attribute' => 'product_id',	
						'value'=>'product.name',
					],			 
					[
						'attribute' => 'option_id',	
						'value'=>'option.name',
					],
					'value',
					/*[
						'attribute' => 'value',
						'content' => function($data){
							return $data->getValeDescription();
						}
					],*/
					[
						'attribute' => 'user_id',
						'value'=>'user.fio',
					],
					[
				        'class' => 'kartik\grid\ActionColumn',
				        'template' => '{update} {delete}',
				        'dropdown' => false,
				        'vAlign'=>'middle',
				        'urlCreator' => function($action, $model, $key, $index) { 
				                return Url::to([$action,'id'=>$key]);
				        },
				        'updateOptions'=>['role'=>'modal-remote','title'=>'Изменить', 'data-toggle'=>'tooltip'],
				        'deleteOptions'=>['role'=>'modal-remote','title'=>'Удалить', 
				                          'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
				                          'data-request-method'=>'post',
				                          'data-toggle'=>'tooltip',
				                          'data-confirm-title'=>'Подтвердите действие',
				                          'data-confirm-message'=>'Вы уверены что хотите удалить этого элемента?'], 
				    ],
		        ],
	            'toolbar'=> '',          
	            'striped' => true,
	            'condensed' => true,
	            'responsive' => true,          
	            'panel' => [
	                'type' => 'primary', 
	                'heading' => '<i class="glyphicon glyphicon-list"></i> Документ',
	                'before'=>'',
	                'after'=>'',
	            ]
	        ])?>

<?php Modal::begin([
    "id"=>"ajaxCrudModal",
    "footer"=>"",// always need it for jquery plugin
])?>
<?php Modal::end(); ?>
