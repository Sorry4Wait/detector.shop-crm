<?php

/* @var $this yii\web\View */
/* @var $model app\models\Parameter */
?>
<div class="parameter-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
