<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Parameter */
?>
<div class="parameter-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            [
                'label' => 'Предприятие',
                'value' => function (\app\models\Parameter $parameter) {
                    return $parameter->object->name;
                },
            ],
            //'object_id',
            //'user_id',
            [
                'label' => 'Пользователь',
                'value' => function (\app\models\Parameter $parameter) {
                    return $parameter->user->name;
                },
            ],
            //'equipment_id',
            [
                'label' => 'Оборудование',
                'value' => function (\app\models\Parameter $parameter) {
                    return $parameter->equipment->name;
                },
            ],
            'date',
            //'option_id',
            [
                'label' => 'Параметр',
                'value' => function (\app\models\Parameter $parameter) {
                    return $parameter->option->name;
                },
            ],
            'value',
        ],
    ]) ?>

</div>
