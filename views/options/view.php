<?php

use yii\widgets\DetailView;

?>
<div class="options-view">
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            //'id',
            'name',
            'valueTypeLabel',
        ],
    ]) ?>
</div>
