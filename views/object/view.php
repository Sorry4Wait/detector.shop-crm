<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\OurObject */

?>
<div class="object-view">
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name',
        ],
    ]) ?>
</div>
