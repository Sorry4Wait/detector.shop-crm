<?php

/* @var $this yii\web\View */
/* @var $model app\models\OurObject */

?>
<div class="object-update">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
