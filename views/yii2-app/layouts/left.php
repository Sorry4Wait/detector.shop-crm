<aside class="main-sidebar">

    <section class="sidebar">

        <?php 
        if(isset(Yii::$app->user->identity->id))
            {
//              $role = Yii::$app->user->identity->role_id;
                echo dmstr\widgets\Menu::widget(

                    [
                        'options' => ['class' => 'sidebar-menu tree', 'data-widget'=> 'tree'],
                        'items' => [
                            ['label' => 'Menu', 'options' => ['class' => 'header']],
                            ['label' => 'Отчеты', 'icon' => 'archive', 'url' => ['/orders']],
                            ['label' => 'Журнал', 'icon' => 'cube', 'url' => ['/journal']],
                            ['label' => 'Элементы', 'icon' => 'cube', 'url' => ['/element']],
                            ['label' => 'Параметры', 'icon' => 'cube', 'url' => ['/parameter']],
                            [
                                'label' => 'Справочники',
                                'icon' => 'book',
                                'visible' => Yii::$app->user->identity->role_id == 1 ? true : false,
                                'url' => '#',
                                'items' => [
                                    ['label' => 'Пользователи', 'icon' => 'file-text-o', 'url' => ['/users'],],
                                    ['label' => 'Объекты', 'icon' => 'file-text-o', 'url' => ['/object'],],
                                    ['label' => 'Оборудование', 'icon' => 'file-text-o', 'url' => ['/equipment'],],
                                    ['label' => 'Продукт', 'icon' => 'file-text-o', 'url' => ['/product'],],
                                    ['label' => 'Параметры', 'icon' => 'file-text-o', 'url' => ['/options'],],
                                    ['label' => 'Тип работ', 'icon' => 'file-text-o', 'url' => ['/type'],],
                                ],
                            ],
                        ],
                    ]
                );
            } 
            else{
                echo dmstr\widgets\Menu::widget(
                    [
                        'options' => ['class' => 'sidebar-menu tree', 'data-widget'=> 'tree'],
                        'items' => [
                            ['label' => 'Menu', 'options' => ['class' => 'header']],
                            ['label' => 'Signup', 'icon' => 'sign-in', 'url' => ['/signup']],                            
                        ],
                    ]
                );
            }
        ?>

    </section>

</aside>
