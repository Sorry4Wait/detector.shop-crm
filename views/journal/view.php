<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Journal */
?>
<div class="journal-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            //'object_id',
            'objectName',
            //'equipment_id',
            'equipmentName',
            //'user_id',
            'date',
            //'type_id',
            [
                'attribute' => 'type_id',
                'value' => $model->type->name,
            ],
            'description',
            'media',
            'node_name',
        ],
    ]) ?>

</div>
