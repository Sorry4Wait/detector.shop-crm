<?php

use yii\helpers\Url;

return [
    [
        'class' => 'kartik\grid\CheckboxColumn',
        'width' => '20px',
    ],
    [
        'class' => 'kartik\grid\SerialColumn',
        'width' => '30px',
    ],
    // [
    // 'class'=>'\kartik\grid\DataColumn',
    // 'attribute'=>'id',
    // ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'object_id',
        'filter' => \app\models\OurObject::getList(),
        'value' => function (\app\models\Journal $journal) {
            return $journal->objectName;
        }
    ]
    ,
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'equipment_id',
        'filter' => \app\models\Equipment::getList(),
        'value' => function (\app\models\Journal $journal) {
            return $journal->equipmentName;
        }
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'type_id',
        'filter' => \app\models\Type::getList(),
        'value' => function (\app\models\Journal $journal) {
            return $journal->typeName;
        }
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'user_id',
        'filter' => \app\models\Journal::getUsersList(),
        'content' => function($data){
            return $data->user->fio;
        }
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'description',
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'node_name',
    ],

    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'date',
        'format' => ['date', 'Y-MM-d']
    ],

    // [
    // 'class'=>'\kartik\grid\DataColumn',
    // 'attribute'=>'description',
    // ],
    // [
    // 'class'=>'\kartik\grid\DataColumn',
    // 'attribute'=>'media',
    // ],
    [
        'class' => 'kartik\grid\ActionColumn',
        'dropdown' => false,
        'vAlign' => 'middle',
        'urlCreator' => function ($action, $model, $key, $index) {
            return Url::to([$action, 'id' => $key]);
        },
        'viewOptions' => ['role' => 'modal-remote', 'title' => 'Просмотр', 'data-toggle' => 'tooltip'],
        'updateOptions' => ['role' => 'modal-remote', 'title' => 'Изменить', 'data-toggle' => 'tooltip'],
        'deleteOptions' => ['role' => 'modal-remote', 'title' => 'Удалить',
            'data-confirm' => false, 'data-method' => false,// for overide yii data api
            'data-request-method' => 'post',
            'data-toggle' => 'tooltip',
            'data-confirm-title' => 'Подтвердите действие',
            'data-confirm-message' => 'Вы уверены что хотите удалить этого элемента?'],
    ],

];