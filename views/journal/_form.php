<?php

use yii\helpers\Html;
use kartik\date\DatePicker;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Journal */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="journal-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'object_id')->label()->widget(\kartik\select2\Select2::classname(), [
                'data' => \app\models\OurObject::getList(),
                'options' => [
                    //'value' =>  $model->type,
                    'placeholder' => 'Выберите предприятие',
                    'onchange'=>'
                        $.post( "/journal/lists?id='.'"+$(this).val(), function( data ){
                            $( "select#journal-equipment_id" ).html( data);
                        });
                    ' 
                ],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ]); ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'equipment_id')->label()->widget(\kartik\select2\Select2::classname(), [
                'data' => $model->getEquipmentList($model->object_id),//\app\models\Equipment::getList(),
                'options' => [
                    'placeholder' => 'Выберите оборудование',
                ],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ]); ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-4">
            <?= $form->field($model, 'date')->widget(DatePicker::classname(), [
                    'options' => ['placeholder' => 'Выберите',],
                    'layout' => '{picker}{input}',
                    'pluginOptions' => [
                        'autoclose'=>true,
                        'format' => 'yyyy-mm-dd',
                        //'startView'=>'year',
                    ]
                ]) ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'type_id')->dropDownList(
                \app\models\Type::getList(), 
                [
                    'prompt' => 'Выберите тип...'
                ]); 
            ?>            
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'media')->textInput(['maxlength' => true]) ?>            
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <?= $form->field($model, 'description')->textarea(['rows' => '3']) ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <?= $form->field($model, 'node_name')->textarea(['rows' => '1']) ?>
        </div>
    </div>


    <?= $form->field($model, 'user_id')
        ->hiddenInput(['value' => Yii::$app->user->identity->id])->label(false); ?>

    <?php if (!Yii::$app->request->isAjax) { ?>
        <div class="form-group">
            <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>
    <?php } ?>

    <?php ActiveForm::end(); ?>
</div>

<script>
    $(document).ready(function () {
        $('#journal-object_id').change(function () {
            //$('#journal-equipment_id option').hide();
            $('select').find('.equipment_object_' + $('#journal-object_id').val()).show();
        });
    });
</script>