<?php

use yii\helpers\Url;
use app\models\EquipmentObject;
use app\models\OurObject;

return [
    [
        'class' => 'kartik\grid\CheckboxColumn',
        'width' => '20px',
    ],
    [
        'class' => 'kartik\grid\SerialColumn',
        'width' => '30px',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute' => 'name',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute' => 'object_list',
        'content' => function($data){
            $all_object = EquipmentObject::find()->where(['equipment_id' => $data->id])->all();
            $title = "";
            $array = [];
            foreach ($all_object as $value) {
                $array [] = $value->object_id;
            }
            $array = array_unique($array);
            foreach ($array as $value) {
                $name = OurObject::findOne($value)->name;
                $title .= $name . '; ';
            }
            return $title;
        }
    ],
    [
        'class' => 'kartik\grid\ActionColumn',
        'dropdown' => false,
        'vAlign'=>'middle',
        'urlCreator' => function($action, $model, $key, $index) { 
                return Url::to([$action,'id'=>$key]);
        },
        'viewOptions'=>['role'=>'modal-remote','title'=>'Просмотр','data-toggle'=>'tooltip'],
        'updateOptions'=>['role'=>'modal-remote','title'=>'Изменить', 'data-toggle'=>'tooltip'],
        'deleteOptions'=>['role'=>'modal-remote','title'=>'Удалить', 
                          'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
                          'data-request-method'=>'post',
                          'data-toggle'=>'tooltip',
                          'data-confirm-title'=>'Подтвердите действие',
                          'data-confirm-message'=>'Вы уверены что хотите удалить этого элемента?'], 
    ],

];   