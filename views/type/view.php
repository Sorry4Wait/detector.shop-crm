<?php

use yii\widgets\DetailView;

?>
<div class="type-view">
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name',
        ],
    ]) ?>
</div>
